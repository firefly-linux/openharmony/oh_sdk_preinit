#!/bin/bash

DEVICES_CONFIGS=(
	# board soc tag_release release
        "ROC-RK3568-PC-HDMI rk3568 OpenHarmony-v3.1.1-Release OpenHarmony-3.1-Release"
        "ROC-RK3568-PC-MIPI rk3568 OpenHarmony-v3.1.1-Release OpenHarmony-3.1-Release"
        "AIO-RK3568J-HDMI rk3568 OpenHarmony-v3.1.1-Release OpenHarmony-3.1-Release"
        "AIO-RK3568J-MIPI rk3568 OpenHarmony-v3.1.1-Release OpenHarmony-3.1-Release"
        # "AIO-RK3399J rk3399 openharmony-3.2-release"
)

patch()
{
	local firefly_remote=firefly
	local firefly_branch=firefly
	local soc=$1
	local board=$2
	local tag_release=$3
	local release=$4

	case $soc in
		rk3568)
			if [ "$release" == "OpenHarmony-3.1-Release" ];then
				# device
				pushd device/hihope
				if git remote -v | grep -q $firefly_remote; then
					git remote remove $firefly_remote
				fi
				git checkout .
				git remote add $firefly_remote https://gitlab.com/firefly-linux/openharmony/device_board_firefly.git
				git remote update $firefly_remote
				if git branch | grep -q $firefly_branch;then
					git checkout $firefly_remote/$soc/$release
					git branch -D $firefly_branch
				fi
				git checkout -b $firefly_branch $firefly_remote/$soc/$release
				popd

				# vendor
				pushd vendor/hihope
				if git remote -v | grep -q $firefly_remote; then
					git remote remove $firefly_remote
				fi
				git checkout .
				git remote add $firefly_remote https://gitlab.com/firefly-linux/openharmony/vendor_firefly.git
				git remote update $firefly_remote
				if git branch | grep -q $firefly_branch;then
					git checkout $firefly_remote/$soc/$release
					git branch -D $firefly_branch
				fi
				git checkout -b $firefly_branch $firefly_remote/$soc/$release
				popd

				# select board
				pushd device/hihope/rk3568/kernel	
				if [ "$board" == "AIO-RK3568J-HDMI" ];then
					ln -sf aio-rk3568j-hdmi.gn BUILD.gn
				elif [ "$board" == "ROC-RK3568-PC-HDMI" ];then
					ln -sf roc-rk3568-pc-hdmi.gn BUILD.gn
				elif [ "$board" == "AIO-RK3568J-MIPI" ];then
					ln -sf aio-rk3568j-mipi.gn BUILD.gn
				elif [ "$board" == "ROC-RK3568-PC-MIPI" ];then
					ln -sf roc-rk3568-pc-mipi.gn BUILD.gn
				fi
				popd

			fi
			;;
	esac
}

usage()
{
	echo -e "Usage:"
	echo -e "\t$0 -l:         list supported boards"
	echo -e "\t$0 -b <board>: select target boards"
	echo -e "\t$0 -p:	  patches to openharmony sdk"
}

while getopts lb:p opt;do
    case $opt in
	l)
		echo -e "Supported devices:"
		for config in "${DEVICES_CONFIGS[@]}"; do
			board=$(echo $config | cut -d " " -f 1)
			echo -e "\t$board"
		done
		exit 0
		;;
	b)
		target_board=$OPTARG
		for config in "${DEVICES_CONFIGS[@]}"; do
			board=$(echo $config | cut -d " " -f 1)
			soc=$(echo $config | cut -d " " -f 2)
			tag_release=$(echo $config | cut -d " " -f 3)
			release=$(echo $config | cut -d " " -f 4)
			if [ "$target_board" == "$board" ];then
				echo "$board" > .board	
				echo "$soc" > .soc
				echo "$tag_release" > .tag_release
				echo "$release" > .release
				exit 0
			fi
		done
		usage
		exit -1
		;;
	p)
		if [ -e .board ] && [ -e .soc ] && [ -e .release ] && [ -e .tag_release ];then
			board=$(cat .board)
			soc=$(cat .soc)
			tag_release=$(cat .tag_release)
			release=$(cat .release)
		else
			echo -e "please use -b option to select target board"
			exit -1
		fi
		if [ -z "$board" ] || [ -z "$soc" ] || [ -z "$release" ] || [ -z "$tag_release" ];then
			echo -e "please use -b option to select target board"
			exit -1
		fi
		set -x
		patch $soc $board $tag_release $release
		set +x
		exit 0
		;;
	*)
		usage
		exit -1
		;;
    esac
done

usage
